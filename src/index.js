import React from 'react';
import ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';
// import { createStore } from 'redux';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

if ('scrollRestoration' in window.history) {
  window.history.scrollRestoration = 'manual';
}
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
