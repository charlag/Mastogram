import React, { Component, Fragment } from 'react';
import './App.css';
import Posts from './components/Posts.js';

const DEFAULT_TAG = 'photography';

const baseUrl = "https://mastodon.social";
const apiUrl = baseUrl +"/api/v1";

class App extends Component {

  _auth() {
    const storage = window.localStorage;
    const client_key = storage.getItem(baseUrl + '/client_key');
    let onAppRegistered;
    if (!client_key) {
      const formData = new FormData();
      formData.append("client_name", "Mastogram");
      formData.append("redirect_uris", window.location);
      formData.append('scopes', 'read write');
      onAppRegistered = fetch(apiUrl + "/apps", {method: 'POST', body: formData})
        .then(response => response.json())
        .then(json => {
          const client_key = json['client_id'];
          const client_secret = json['client_secret'];
          storage.setItem(baseUrl + '/client_key', client_key);
          storage.setItem(baseUrl + '/client_secret', client_secret);
          return {client_key, client_secret};
        })
    } else {
      onAppRegistered = Promise.resolve({
        client_key,
        client_secret: storage.getItem('client_secret')
      });
    }
    onAppRegistered.then(({client_key, client_secret}) => {
      const redirect = baseUrl + "/oauth/authorize" +
            "?client_id=" + encodeURIComponent(client_key) +
            "&scope=" + "read%20write" +
            "&response_type=" + "code" +
            "&redirect_uri=" + encodeURIComponent(window.location.href);
      window.location.href = redirect;
    })
  }

  componentWillMount() {
    const params = new URLSearchParams(window.location.search.slice(1));
    const tag = params.get('tag') || DEFAULT_TAG;
    const accessToken = window.localStorage.getItem('access_token');
    params.set('tag', tag);

    this.setState({tag: tag, accessToken});

    const code = params.get('code');
    if (code && !accessToken) {
      window.localStorage.setItem('refresh_token', code);
      window.history.replaceState(null, null, window.location.pathname)
      this._finishAuth(code);
    }
  }

  _handleAuthButton() {
    if (this.state.accessToken) {
      this._logout();
    } else {
      this._auth();
    }
  }

  _logout() {
    const storage = window.localStorage;
    storage.removeItem('access_token');
    storage.removeItem('refresh_token');
    this.setState({accessToken: null});
  }

  _finishAuth(refreshToken) {
    const storage = window.localStorage;
    const params = new FormData();
    const redirectUri = window.location.protocol + '//' + window.location.host +
          window.location.pathname;
    params.append('client_id', storage.getItem(baseUrl + '/client_key'));
    params.append('client_secret', storage.getItem(baseUrl + '/client_secret'));
    params.append('redirect_uri', redirectUri);
    params.append('grant_type', 'authorization_code');
    params.append('code', refreshToken);
    return fetch(baseUrl + '/oauth/token', {
      method: 'POST',
      body: params
    })
      .then(resp => resp.json())
      .then(({access_token}) => {
        storage.setItem('access_token', access_token);
        this.setState({accessToken: access_token})
      })
      .catch(e => console.error(e, "Failed to get access token"));
  }

  render() {
    return (
        <Fragment>
        <a className="login-btn" href="#" onClick={() => {this._handleAuthButton()}}>
      {this.state.accessToken ? 'Log out' : 'Log in'}
    </a>
        <h1 className="tagHeader">{'#' + (this.state.tag)}</h1>
        <Posts tag={this.state.tag} apiUri={apiUrl} accessToken={this.state.accessToken}/>
        </Fragment>
    );
  }
}

export default App;
