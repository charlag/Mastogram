import React, { Component } from 'react';
import { List, WindowScroller, InfiniteLoader } from 'react-virtualized';
import './Posts.css';
import 'react-virtualized/styles.css';
import _ from 'lodash';
import moment from 'moment';

const MAX_WIDTH = 800;

export default class Posts extends Component {
  state = {
    posts: []
  };

  componentWillMount() {
    this.loadMoreRows({startIndex: 0});
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.accessToken !== nextProps.accessToken) {
      this.setState({posts: []});
    }
  }

  renderItem({
    key,
    index,
    style
  }) {
    const post = this.state.posts[index];
    let heart = null;
    const elProps = {
      className: "post-media",
      onError: (e) => e.target.remove(),
      onDoubleClick: (e) => this._handleDoubleClick(e, index, heart),
      src: post.media_attachments[0].url,
      alt: post.media_attachments[0].description || "no alt text, sorry!",
    }
    const element = post.media_attachments[0].type === 'video' ?
          <video {...elProps} /> :
          <img {...elProps} />;

    return <article className="post-wrapper" key={key} style={style} >
      <div className="post" >

      <div className="post-header">
      <img className="post-avatar" src={post.account.avatar} alt="avatar" />
      <div className="post-names-box">
      <div className="post-display-name">{post.account.display_name}</div>
      <strong className="post-username">{'@' + post.account.acct}</strong>
      </div>
      </div>
      {
        element
      }
      <img className="fav"
    src="star.svg"
    ref={(h) => heart = h}>
    </img>

      <div className="post-text" dangerouslySetInnerHTML={{__html: post.content}} />
      <a className="post-posted-at" href={post.url}>
      {moment(post.created_at).fromNow()}
    </a>
    </div>
    </article>;
  }

  loadMoreRows = _.throttle(({startIndex, stopIndex}) => {
    console.log('loadMoreRows. start ' + startIndex + ' end ' + stopIndex);
    const url = new URL(this.props.apiUri + '/timelines/tag/' + this.props.tag);
    url.searchParams.append('only_media', true);
    const len = this.state.posts.length;
    if (len > 0) {
      url.searchParams.append('max_id', this.state.posts[len - 1].id);
    }
    if (this.state.maxRequested > this.state.length) {
      console.log('maxRequested is bigger: ' + this.state.maxRequested);
      return Promise.resolve();
    }
    this.setState({maxRequested: this.state.posts.length});
    return fetch(url,
                 { headers: 
                   {'Authorization': `Bearer ${this.props.accessToken}`}
                 })
      .then(response => response.json())
      .then(posts => {
        this.setState({posts: [...this.state.posts, ...posts]});
        console.log('new posts: ' + posts.length);
      });
  }, 1500, {trailing: false});

  _handleDoubleClick(e, index, heart) {
    console.log("DOUBLE CLICK " + index);
    const post = this.state.posts[index];
    if (!post.favourited) {
      heart.classList.add('animate-alpha');
      setTimeout(() => {
        heart.classList.remove('animate-alpha');
      }, 5000);
    }
    fetch(this.props.apiUri + `/statuses/${post.id}/${post.favourited ?
                     'unfavourite' : 'favourite'}`,
          {
            method: 'POST',
            headers: {
              'Authorization': `Bearer ${this.props.accessToken}`
            }
          });
  }

  isRowLoaded({index}) {
    return index < this.state.posts.length;
  }

  render() {
    return <InfiniteLoader
    isRowLoaded={this.isRowLoaded.bind(this)}
    loadMoreRows={this.loadMoreRows.bind(this)}
    threshold={5}
    rowCount={9999} >
      {({onRowsRendered, registerChild}) => <WindowScroller>
       {({ width, height, isScrolling, onChildScroll, scrollTop }) => (
         <List
           className="posts"
           autoHeight
           height={height}
           width={width < MAX_WIDTH ? width : MAX_WIDTH}
                          rowCount={this.state.posts.length}
         rowHeight={width < MAX_WIDTH ? 600 : 800}
                          rowRenderer={this.renderItem.bind(this)}
                          onRowsRendered={onRowsRendered}
                          ref={registerChild}
                          isScrolling={isScrolling}
                          onScroll={onChildScroll}
                          scrollTop={scrollTop}
                          />
       )}
       </WindowScroller>
      }
       </InfiniteLoader>
  }
}
